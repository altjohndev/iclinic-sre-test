#!/bin/bash

until nc -z ${PAYMENTS_API__RABBITMQ_HOST:-localhost} ${PAYMENTS_API__RABBITMQ_PORT:-5672}; do
  echo "$(date) - waiting for rabbitmq..."
  sleep 2
done

until nc -z ${PAYMENTS_API__POSTGRES_HOST:-localhost} ${PAYMENTS_API__POSTGRES_PORT:-5432}; do
  echo "$(date) - waiting for postgres..."
  sleep 2
done

alembic upgrade head
nameko run --config nameko.yml payments.service --backdoor 3000
