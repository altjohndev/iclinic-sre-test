import uuid
from datetime import datetime

from sqlalchemy import DECIMAL, Column
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import declarative_base

DeclarativeBase = declarative_base()


class AppointmentPaymentRequest(DeclarativeBase):
    """
    Model used to represent a payment request for a finished appointment between patient and physician.

    Attributes
    ----------
    appointment_id : uuid
        the appointment identifier and the primary key of this model
    total_price : Decimal
        the price for the appointment
    """

    __tablename__ = "appointments_payment_requests"

    appointment_id = Column(UUID(as_uuid=True), primary_key=True)

    total_price = Column(DECIMAL(18, 2))
