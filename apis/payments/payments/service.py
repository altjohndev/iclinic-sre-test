from datetime import datetime

from nameko.events import EventDispatcher, event_handler
from nameko.rpc import rpc
from nameko_sqlalchemy import DatabaseSession
from sqlalchemy.exc import IntegrityError

from payments.exceptions import AppointmentPaymentRequestNotFound
from payments.models import AppointmentPaymentRequest, DeclarativeBase
from payments.schemas import AppointmentPaymentRequestSchema
from payments.wrappers import safe_return


class PaymentsService:
    """
    Service responsible to manage payments data.

    Methods
    -------
    list_appointments_payment_requests()
        Lists all registered appointments payment requests
    get_appointment_payment_request()
        Retrieves a registered appointment payment request based on appointment_id
    handle_appointment_payment_request_received(payload)
        Registers a appointment payment request based on the finished appointment.
        Also, broadcasts the request if sucessfully registered it
    """

    name = "payments"

    db = DatabaseSession(DeclarativeBase)
    event_dispatcher = EventDispatcher()

    @rpc
    @safe_return
    def list_appointments_payment_requests(self) -> dict:
        requests = self.db.query(AppointmentPaymentRequest).all()
        return AppointmentPaymentRequestSchema(many=True).dump(requests)

    @rpc
    @safe_return
    def get_appointment_payment_request(self, appointment_id: str) -> dict:
        request = self.db.query(AppointmentPaymentRequest).get(appointment_id)

        if not request:
            raise AppointmentPaymentRequestNotFound(appointment_id)

        return AppointmentPaymentRequestSchema().dump(request)

    @event_handler("appointments", "appointment_finished")
    def handle_appointment_finished(self, payload: dict) -> None:
        request = AppointmentPaymentRequest(appointment_id=payload["id"], total_price=payload["price"])

        try:
            self.db.add(request)
            self.db.commit()

            print(f"[{datetime.utcnow()}] Appointment payment request created. ID: {request.appointment_id}")
        except IntegrityError:
            print(f"[{datetime.utcnow()}] Appointment payment request already exists. ID: {request.appointment_id}")
            self.db.rollback()
        finally:
            request = AppointmentPaymentRequestSchema().dump(request)
            self.event_dispatcher("appointment_payment_request_received", request)
