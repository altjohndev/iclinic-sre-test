class AppointmentPaymentRequestNotFound(Exception):
    """Raised when appointment_id does not represent any appointment payment request in database."""

    def __init__(self, appointment_id: str) -> None:
        self.message = f'Appointment payment request not found. ID: "{appointment_id}"'
