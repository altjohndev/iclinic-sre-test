import functools
from datetime import datetime
from typing import Callable

from marshmallow.exceptions import ValidationError

from payments.exceptions import AppointmentPaymentRequestNotFound


def valid_data(data) -> dict:
    """Wraps data with has_errors as False. Root keys are: has_errors and data."""
    return {"has_errors": False, "data": data}


def invalid_data(type, data) -> dict:
    """Wraps data with has_errors as True. Root keys are: has_errors, error_type, and error_data."""
    return {"has_errors": True, "error_type": type, "error_data": data}


def safe_return(function: Callable) -> Callable:
    """Decorates a function to wrap data returned. Also handles exceptions."""

    @functools.wraps(function)
    def wrapper(*args, **kwargs) -> Callable:
        try:
            return valid_data(function(*args, **kwargs))
        except AppointmentPaymentRequestNotFound as exception:
            return invalid_data("AppointmentPaymentRequestNotFound", {"message": exception.message})
        except Exception as exception:
            print(
                f"[{datetime.utcnow()}] Failed to handle request: {exception}. "
                f"function: {function}, args: {args}, kwargs: {kwargs}"
            )

            return invalid_data("ServiceError", {"message": str(exception)})

    return wrapper
