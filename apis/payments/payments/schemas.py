from marshmallow import Schema, fields


class AppointmentPaymentRequestSchema(Schema):
    """
    Schema used to represent a payment request for a finished appointment between patient and physician.

    Attributes
    ----------
    id : uuid
        the appointment identifier
    total_price : Decimal
        the price for the appointment
    """

    appointment_id = fields.UUID(required=True)
    total_price = fields.Decimal()
