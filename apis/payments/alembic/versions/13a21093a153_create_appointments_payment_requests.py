"""create appointments payment requests

Revision ID: 13a21093a153
Revises:
Create Date: 2021-06-10 13:28:20.622538

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = "13a21093a153"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "appointments_payment_requests",
        sa.Column("appointment_id", UUID(), nullable=False),
        sa.Column("total_price", sa.DECIMAL(18, 2)),
        sa.PrimaryKeyConstraint("appointment_id"),
    )


def downgrade():
    op.drop_table("appointments_payment_requests")
