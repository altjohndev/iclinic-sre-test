from datetime import datetime
from decimal import Decimal
from uuid import UUID, uuid4

import pytest
from nameko.testing.services import worker_factory
from payments.models import AppointmentPaymentRequest
from payments.service import PaymentsService


@pytest.fixture
def payments_service(db_session):
    return worker_factory(PaymentsService, db=db_session)


def test_list_appointments_payment_requests(payments_service, db_session):
    request = AppointmentPaymentRequest(appointment_id=uuid4(), total_price=Decimal(200))

    db_session.add(request)
    db_session.commit()

    response = payments_service.list_appointments_payment_requests()

    assert not response["has_errors"]
    assert len(response["data"]) == 1
    assert UUID(response["data"][0]["appointment_id"]) == request.appointment_id


def test_get_appointment_payment_request(payments_service, db_session):
    request = AppointmentPaymentRequest(appointment_id=uuid4(), total_price=Decimal(200))

    db_session.add(request)
    db_session.commit()

    response = payments_service.get_appointment_payment_request(request.appointment_id)

    assert not response["has_errors"]
    assert UUID(response["data"]["appointment_id"]) == request.appointment_id


def test_attempt_to_get_inexistent_appointment_payment_request(payments_service):
    response = payments_service.get_appointment_payment_request(str(uuid4()))

    assert response["has_errors"]
    assert response["error_type"] == "AppointmentPaymentRequestNotFound"
    assert "Appointment payment request not found" in response["error_data"]["message"]


def test_unhandled_exception():
    appointments_service = worker_factory(PaymentsService, db=None)
    response = appointments_service.list_appointments_payment_requests()

    assert response["has_errors"]
    assert response["error_type"] == "ServiceError"


def test_handle_appointment_finished(payments_service, db_session):
    appointment_id = uuid4()

    payments_service.handle_appointment_finished({"id": appointment_id, "price": 200})

    assert db_session.query(AppointmentPaymentRequest).get(appointment_id) is not None


def test_handle_duplicated_appointment_finished(payments_service, db_session):
    appointment_id = uuid4()

    payments_service.handle_appointment_finished({"id": appointment_id, "price": 200})
    payments_service.handle_appointment_finished({"id": appointment_id, "price": 200})

    assert db_session.query(AppointmentPaymentRequest).get(appointment_id) is not None
