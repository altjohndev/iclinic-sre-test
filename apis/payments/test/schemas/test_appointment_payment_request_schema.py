from decimal import Decimal

from payments.models import AppointmentPaymentRequest
from payments.schemas import AppointmentPaymentRequestSchema


def test_appointment_schema_dump():
    assert AppointmentPaymentRequestSchema().dump(AppointmentPaymentRequest(total_price=Decimal(200))) == {
        "appointment_id": None,
        "total_price": 200,
    }


def test_appointment_schema_many_dump():
    assert AppointmentPaymentRequestSchema(many=True).dump(
        [AppointmentPaymentRequest(total_price=Decimal(200)), AppointmentPaymentRequest(total_price=Decimal(400))]
    ) == [
        {
            "appointment_id": None,
            "total_price": 200,
        },
        {
            "appointment_id": None,
            "total_price": 400,
        },
    ]
