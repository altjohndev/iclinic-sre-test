from decimal import Decimal
from uuid import UUID, uuid4

import pytest
from nameko_sqlalchemy import DatabaseSession
from payments.models import AppointmentPaymentRequest
from sqlalchemy.exc import IntegrityError, SAWarning


def test_create_appointment_payment_request(db_session: DatabaseSession):
    request = AppointmentPaymentRequest(appointment_id=uuid4(), total_price=Decimal(200))

    db_session.add(request)
    db_session.commit()

    assert isinstance(request.appointment_id, UUID)


def test_attempt_to_create_appointment_payment_request_without_appointment_id(db_session: DatabaseSession):
    request = AppointmentPaymentRequest(total_price=Decimal(200))

    db_session.add(request)

    with pytest.raises(IntegrityError) as exception_info:
        with pytest.warns(SAWarning):
            db_session.commit()

    assert 'null value in column "appointment_id"' in str(exception_info.value)
