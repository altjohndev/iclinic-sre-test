from datetime import datetime
from typing import Callable

from marshmallow.exceptions import ValidationError
from nameko.rpc import rpc
from nameko_sqlalchemy import DatabaseSession

from patients.models import DeclarativeBase, Patient
from patients.schemas import CreatePatientSchema, PatientSchema
from patients.wrappers import safe_return


class PatientsService:
    """
    Service responsible to manage patients data.

    Methods
    -------
    list_patients()
        Lists all registered patients
    create_patient(data)
        Registers a new patient
    """

    name = "patients"

    db = DatabaseSession(DeclarativeBase)

    @rpc
    @safe_return
    def list_patients(self) -> dict:
        patients = self.db.query(Patient).all()
        return PatientSchema(many=True).dump(patients)

    @rpc
    @safe_return
    def create_patient(self, data: dict) -> dict:
        data = CreatePatientSchema().load(data)

        patient = Patient(name=data["name"])

        self.db.add(patient)
        self.db.commit()

        print(f'[{datetime.utcnow()}] Patient "{patient.name}" created. ID: {patient.id}')

        patient = PatientSchema().dump(patient)

        return patient
