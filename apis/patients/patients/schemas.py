from marshmallow import Schema, fields


class PatientSchema(Schema):
    """
    Schema used to represent a person receiving or registered to receive medical treatment.

    Attributes
    ----------
    id : uuid
        the identifier of the schema
    name : str
        the name of the patient
    """

    id = fields.UUID(required=True)
    name = fields.String(required=True)


class CreatePatientSchema(Schema):
    """
    Schema to represent the payload used to create a patient.

    Attributes
    ----------
    name : str
        the name of the patient
    """

    name = fields.String(required=True)
