# iClinic SRE Test - Patients Service

Service responsible to manage patients data.

## Technologies

- [Python](https://www.python.org/) - An interpreted high-level general-purpose programming language.

- [Nameko](https://nameko.readthedocs.io/en/stable/) - A microservices framework for Python that lets service
  developers concentrate on application logic and encourages testability.

- [Marshmallow](https://marshmallow.readthedocs.io/en/stable/) - An ORM/ODM/framework-agnostic library for converting
  complex datatypes, such as objects, to and from native Python datatypes.

- [SQLAlchemy](https://docs.sqlalchemy.org/en/14/) -Aa comprehensive set of tools for working with databases and Python.

- [Alembic](https://alembic.sqlalchemy.org/en/latest/) - Aa lightweight database migration tool for usage with the
  SQLAlchemy Database Toolkit for Python.

## Usage with Docker

You can set up the service using [docker-compose](https://docs.docker.com/compose/):

```yaml
version: '3.8'

services:
  patients:
    image: arkye/iclinic-sre-test:patients
    environment:
      PHYSICIANS_API__RABBITMQ_HOST: rabbitmq
      PHYSICIANS_API__POSTGRES_HOST: postgres
    networks:
      - network
    depends_on:
      - patients_postgres
      - rabbitmq

  patients_postgres:
    image: postgres:13.0
    environment:
      POSTGRES_DB: patients
      POSTGRES_PASSWORD: password
    volumes:
      - postgres:/var/lib/postgresql/data
    ports:
      - 5432:5432
    networks:
      - network

  rabbitmq:
    image: rabbitmq:3.8.17-management
    ports:
      - 5672:5672
      - 15672:15672
    networks:
      - network

networks:
  network:
    name: iclinic_sre_test

volumes:
  postgres:
    name: iclinic_sre_test_patients_postgres
```

To start the services:

```bash
docker-compose up -d
```

To stop the services:

```bash
docker-compose down
```

To access the [nameko shell](https://nameko.readthedocs.io/en/stable/cli.html):

```bash
docker-compose exec patients nameko shell --config nameko.yml
```

To (cautiously) reset the database:

```bash
docker-compose exec patients alembic downgrade base
docker-compose exec patients alembic upgrade head
```

## Shell usage

To create a patient inside the nameko shell:

```python
>>> n.rpc.patients.create_patient({'name': 'John Doe'})
{'has_errors': False, 'data': {'id': 'e1e9c911-1c4b-4a7d-a306-494d633174bd', 'name': 'John Doe'}}
```

To list all patients inside the nameko shell:

```python
>>> n.rpc.patients.list_patients()
{'has_errors': False, 'data': [{'id': 'e1e9c911-1c4b-4a7d-a306-494d633174bd', 'name': 'John Doe'}]}
```

## Environment Variables

### PostgreSQL

- `PATIENTS_API__POSTGRES_USER` - The PostgreSQL user. Defaults to `postgres`
- `PATIENTS_API__POSTGRES_PASSWORD` - The PostgreSQL user password. Defaults to `password`
- `PATIENTS_API__POSTGRES_HOST` - The PostgreSQL hostname. Defaults to `localhost`
- `PATIENTS_API__POSTGRES_PORT` - The PostgreSQL port. Defaults to `5432`
- `PATIENTS_API__POSTGRES_DB` - The PostgreSQL database name. Defaults to `patients`

### RabbitMQ

- `PATIENTS_API__RABBITMQ_USER` - The RabbitMQ user. Defaults to `guest`
- `PATIENTS_API__RABBITMQ_PASSWORD` - The RabbitMQ user password. Defaults to `guest`
- `PATIENTS_API__RABBITMQ_HOST` - The RabbitMQ hostname. Defaults to `localhost`
- `PATIENTS_API__RABBITMQ_PORT` - The RabbitMQ port. Defaults to `5672`

## Development

### Local setup

To install dependencies, use:

```bash
pip install -e .[dev]
```

### Tests

Simply execute the `test.sh` file.
