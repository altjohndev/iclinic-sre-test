from patients.models import Patient
from patients.schemas import PatientSchema


def test_patient_schema_dump():
    assert PatientSchema().dump(Patient(name="John Doe")) == {"id": None, "name": "John Doe"}


def test_patient_schema_many_dump():
    assert PatientSchema(many=True).dump([Patient(name="John Doe"), Patient(name="Joana Doe")]) == [
        {"id": None, "name": "John Doe"},
        {"id": None, "name": "Joana Doe"},
    ]
