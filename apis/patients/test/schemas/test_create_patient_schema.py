import pytest
from marshmallow.exceptions import ValidationError
from patients.schemas import CreatePatientSchema


def test_create_patient_schema_load():
    assert CreatePatientSchema().load({"name": "John Doe"}) == {"name": "John Doe"}


def test_create_patient_schema_load_without_name():
    with pytest.raises(ValidationError) as exception_info:
        CreatePatientSchema().load({})
    assert "'name'" in str(exception_info.value)
