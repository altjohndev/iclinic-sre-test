from uuid import UUID

import pytest
from nameko_sqlalchemy import DatabaseSession
from patients.models import Patient
from sqlalchemy.exc import IntegrityError


def test_create_patient(db_session: DatabaseSession):
    patient = Patient(name="John Doe")

    db_session.add(patient)
    db_session.commit()

    assert isinstance(patient.id, UUID)


def test_attempt_to_create_patient_without_name(db_session: DatabaseSession):
    patient = Patient()

    db_session.add(patient)

    with pytest.raises(IntegrityError) as exception_info:
        db_session.commit()

    assert 'null value in column "name"' in str(exception_info.value)
