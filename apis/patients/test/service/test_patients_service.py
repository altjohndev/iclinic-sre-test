from uuid import UUID

import pytest
from nameko.testing.services import worker_factory
from patients.models import Patient
from patients.service import PatientsService


@pytest.fixture
def patient(db_session):
    patient = Patient(name="John Doe")

    db_session.add(patient)
    db_session.commit()

    return patient


@pytest.fixture
def patients_service(db_session):
    return worker_factory(PatientsService, db=db_session)


def test_list_patients(patients_service, patient):
    response = patients_service.list_patients()
    assert not response["has_errors"]
    assert len(response["data"]) == 1
    assert UUID(response["data"][0]["id"]) == patient.id


def test_create_patient(patients_service):
    response = patients_service.create_patient({"name": "Joana Doe"})
    assert not response["has_errors"]
    assert isinstance(UUID(response["data"]["id"]), UUID)


def test_create_patient_without_name(patients_service):
    response = patients_service.create_patient({})
    assert response["has_errors"]
    assert response["error_type"] == "ValidationError"
    assert "name" in response["error_data"]


def test_unhandled_exception():
    patients_service = worker_factory(PatientsService, db=None)
    response = patients_service.list_patients()
    assert response["has_errors"]
    assert response["error_type"] == "ServiceError"
