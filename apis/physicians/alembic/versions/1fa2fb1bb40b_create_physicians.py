"""create physicians

Revision ID: 1fa2fb1bb40b
Revises: d95f14b940ff
Create Date: 2021-06-10 12:04:04.346640

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = "1fa2fb1bb40b"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "physicians",
        sa.Column("id", UUID(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade():
    op.drop_table("physicians")
