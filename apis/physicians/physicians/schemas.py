from marshmallow import Schema, fields


class PhysicianSchema(Schema):
    """
    Schema used to represent a person qualified to practice medicine.

    Attributes
    ----------
    id : uuid
        the identifier of the schema
    name : str
        the name of the physician
    """

    id = fields.UUID(required=True)
    name = fields.String(required=True)


class CreatePhysicianSchema(Schema):
    """
    Schema to represent the payload used to create a physician.

    Attributes
    ----------
    name : str
        the name of the physician
    """

    name = fields.String(required=True)
