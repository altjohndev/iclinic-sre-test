from datetime import datetime
from typing import Callable

from marshmallow.exceptions import ValidationError
from nameko.rpc import rpc
from nameko_sqlalchemy import DatabaseSession

from physicians.models import DeclarativeBase, Physician
from physicians.schemas import CreatePhysicianSchema, PhysicianSchema
from physicians.wrappers import safe_return


class PhysiciansService:
    """
    Service responsible to manage physicians data.

    Methods
    -------
    list_physicians()
        Lists all registered physicians
    create_physician(data)
        Registers a new physician
    """

    name = "physicians"

    db = DatabaseSession(DeclarativeBase)

    @rpc
    @safe_return
    def list_physicians(self) -> dict:
        physicians = self.db.query(Physician).all()
        return PhysicianSchema(many=True).dump(physicians)

    @rpc
    @safe_return
    def create_physician(self, data: dict) -> dict:
        data = CreatePhysicianSchema().load(data)

        physician = Physician(name=data["name"])

        self.db.add(physician)
        self.db.commit()

        print(f'[{datetime.utcnow()}] Physician "{physician.name}" created. ID: {physician.id}')

        physician = PhysicianSchema().dump(physician)

        return physician
