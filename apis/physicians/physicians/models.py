import uuid
from datetime import datetime

from sqlalchemy import Column, DateTime, String
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import declarative_base

DeclarativeBase = declarative_base()


class Physician(DeclarativeBase):
    """
    Model used to represent a person qualified to practice medicine.

    Attributes
    ----------
    id : uuid
        the primary key of the model
    name : str
        the name of the physician
    """

    __tablename__ = "physicians"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String, nullable=False)
