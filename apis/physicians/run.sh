#!/bin/bash

until nc -z ${PHYSICIANS_API__RABBITMQ_HOST:-localhost} ${PHYSICIANS_API__RABBITMQ_PORT:-5672}; do
  echo "$(date) - waiting for rabbitmq..."
  sleep 2
done

until nc -z ${PHYSICIANS_API__POSTGRES_HOST:-localhost} ${PHYSICIANS_API__POSTGRES_PORT:-5432}; do
  echo "$(date) - waiting for postgres..."
  sleep 2
done

alembic upgrade head
nameko run --config nameko.yml physicians.service --backdoor 3000
