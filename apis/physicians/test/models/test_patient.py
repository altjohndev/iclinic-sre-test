from uuid import UUID

import pytest
from nameko_sqlalchemy import DatabaseSession
from physicians.models import Physician
from sqlalchemy.exc import IntegrityError


def test_create_physician(db_session: DatabaseSession):
    physician = Physician(name="John Doe")

    db_session.add(physician)
    db_session.commit()

    assert isinstance(physician.id, UUID)


def test_attempt_to_create_physician_without_name(db_session: DatabaseSession):
    physician = Physician()

    db_session.add(physician)

    with pytest.raises(IntegrityError) as exception_info:
        db_session.commit()

    assert 'null value in column "name"' in str(exception_info.value)
