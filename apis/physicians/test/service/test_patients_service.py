from uuid import UUID

import pytest
from nameko.testing.services import worker_factory
from physicians.models import Physician
from physicians.service import PhysiciansService


@pytest.fixture
def physician(db_session):
    physician = Physician(name="John Doe")

    db_session.add(physician)
    db_session.commit()

    return physician


@pytest.fixture
def physicians_service(db_session):
    return worker_factory(PhysiciansService, db=db_session)


def test_list_physicians(physicians_service, physician):
    response = physicians_service.list_physicians()
    assert not response["has_errors"]
    assert len(response["data"]) == 1
    assert UUID(response["data"][0]["id"]) == physician.id


def test_create_physician(physicians_service):
    response = physicians_service.create_physician({"name": "Joana Doe"})
    assert not response["has_errors"]
    assert isinstance(UUID(response["data"]["id"]), UUID)


def test_create_physician_without_name(physicians_service):
    response = physicians_service.create_physician({})
    assert response["has_errors"]
    assert response["error_type"] == "ValidationError"
    assert "name" in response["error_data"]


def test_unhandled_exception():
    physicians_service = worker_factory(PhysiciansService, db=None)
    response = physicians_service.list_physicians()
    assert response["has_errors"]
    assert response["error_type"] == "ServiceError"
