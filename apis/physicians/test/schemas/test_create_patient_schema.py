import pytest
from marshmallow.exceptions import ValidationError
from physicians.schemas import CreatePhysicianSchema


def test_create_physician_schema_load():
    assert CreatePhysicianSchema().load({"name": "John Doe"}) == {"name": "John Doe"}


def test_create_physician_schema_load_without_name():
    with pytest.raises(ValidationError) as exception_info:
        CreatePhysicianSchema().load({})
    assert "'name'" in str(exception_info.value)
