from physicians.models import Physician
from physicians.schemas import PhysicianSchema


def test_physician_schema_dump():
    assert PhysicianSchema().dump(Physician(name="John Doe")) == {"id": None, "name": "John Doe"}


def test_physician_schema_many_dump():
    assert PhysicianSchema(many=True).dump([Physician(name="John Doe"), Physician(name="Joana Doe")]) == [
        {"id": None, "name": "John Doe"},
        {"id": None, "name": "Joana Doe"},
    ]
