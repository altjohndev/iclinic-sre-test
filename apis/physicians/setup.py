#!/usr/bin/env python

from setuptools import find_packages, setup

REQUIREMENTS = (
    "alembic==1.6.5",
    "marshmallow==3.12.1",
    "nameko-sqlalchemy==1.5.0",
    "nameko==2.13.0",
    "psycopg2-binary==2.8.6",
)

DEV_REQUIREMENTS = (
    "autoflake==1.4",
    "black==21.5b2",
    "coverage==5.5",
    "pycodestyle==2.7.0",
    "pylint==2.8.3",
    "pytest==6.2.4",
)

setup(
    name="iClinic SRE Test - Physicians Service",
    version="0.0.1",
    packages=find_packages(),
    python_requires=">=3.9",
    install_requires=REQUIREMENTS,
    extras_require={"dev": DEV_REQUIREMENTS},
    zip_safe=True,
)
