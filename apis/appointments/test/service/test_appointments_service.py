from datetime import datetime
from decimal import Decimal
from uuid import UUID, uuid4

import pytest
from appointments.models import Appointment, PendingAppointmentPaymentRequest
from appointments.service import AppointmentsService
from nameko.testing.services import worker_factory


@pytest.fixture
def appointments_service(db_session):
    return worker_factory(AppointmentsService, db=db_session)


def test_list_appointments(appointments_service, db_session):
    appointment = Appointment(patient_id=uuid4(), physician_id=uuid4())

    db_session.add(appointment)
    db_session.commit()

    response = appointments_service.list_appointments()

    assert not response["has_errors"]
    assert len(response["data"]) == 1
    assert UUID(response["data"][0]["id"]) == appointment.id


def test_list_pending_appointments_payment_requests(appointments_service, db_session):
    request = PendingAppointmentPaymentRequest(appointment_id=uuid4())

    db_session.add(request)
    db_session.commit()

    response = appointments_service.list_pending_appointments_payment_requests()

    assert not response["has_errors"]
    assert len(response["data"]) == 1
    assert UUID(response["data"][0]["appointment_id"]) == request.appointment_id


def test_start_appointment(appointments_service):
    response = appointments_service.start_appointment(
        {"patient_id": str(uuid4()), "physician_id": str(uuid4()), "timestamp": datetime.utcnow().isoformat()}
    )

    assert not response["has_errors"]
    assert isinstance(UUID(response["data"]["id"]), UUID)
    assert isinstance(datetime.fromisoformat(response["data"]["start_date"]), datetime)


def test_start_appointment_without_patient_id(appointments_service):
    response = appointments_service.start_appointment(
        {"physician_id": str(uuid4()), "timestamp": datetime.utcnow().isoformat()}
    )
    assert response["has_errors"]
    assert response["error_type"] == "ValidationError"
    assert "patient_id" in response["error_data"]


def test_start_appointment_without_physician_id(appointments_service):
    response = appointments_service.start_appointment(
        {"patient_id": str(uuid4()), "timestamp": datetime.utcnow().isoformat()}
    )
    assert response["has_errors"]
    assert response["error_type"] == "ValidationError"
    assert "physician_id" in response["error_data"]


def test_end_appointment(appointments_service):
    response = appointments_service.start_appointment(
        {"patient_id": str(uuid4()), "physician_id": str(uuid4()), "timestamp": datetime.utcnow().isoformat()}
    )

    appointment_id = response["data"]["id"]

    response = appointments_service.end_appointment({"id": appointment_id, "timestamp": datetime.utcnow().isoformat()})

    assert not response["has_errors"]
    assert response["data"]["id"] == appointment_id
    assert isinstance(datetime.fromisoformat(response["data"]["end_date"]), datetime)
    assert isinstance(Decimal(response["data"]["price"]), Decimal)


def test_end_appointment_without_id(appointments_service):
    response = appointments_service.end_appointment({"timestamp": datetime.utcnow().isoformat()})

    assert response["has_errors"]
    assert response["error_type"] == "ValidationError"
    assert "id" in response["error_data"]


def test_end_appointment_with_unknown_id(appointments_service):
    response = appointments_service.end_appointment({"id": str(uuid4()), "timestamp": datetime.utcnow().isoformat()})

    assert response["has_errors"]
    assert response["error_type"] == "AppointmentNotFound"
    assert "Appointment not found" in response["error_data"]["message"]


def test_unhandled_exception():
    appointments_service = worker_factory(AppointmentsService, db=None)
    response = appointments_service.list_appointments()
    assert response["has_errors"]
    assert response["error_type"] == "ServiceError"


def test_handle_appointment_payment_request_received(appointments_service, db_session):
    appointment_id = uuid4()

    request = PendingAppointmentPaymentRequest(appointment_id=appointment_id)

    db_session.add(request)
    db_session.commit()

    appointments_service.handle_appointment_payment_request_received({"appointment_id": appointment_id})

    assert db_session.query(PendingAppointmentPaymentRequest).get(appointment_id) is None


def test_reattempt_to_send_appointment_payment_request(appointments_service, db_session):
    appointment = Appointment(patient_id=uuid4(), physician_id=uuid4())

    db_session.add(appointment)
    db_session.commit()

    appointment_id = appointment.id

    last_reattempt_at = datetime(2021, 1, 1)

    request = PendingAppointmentPaymentRequest(appointment_id=appointment_id, last_reattempt_at=last_reattempt_at)

    db_session.add(request)
    db_session.commit()

    appointments_service.reattempt_to_send_appointment_payment_request()

    request = db_session.query(PendingAppointmentPaymentRequest).get(appointment_id)

    assert request.reattempts == 1
    assert request.last_reattempt_at > last_reattempt_at


def test_not_reattempt_to_send_appointment_payment_request_if_too_soon(appointments_service, db_session):
    appointment = Appointment(patient_id=uuid4(), physician_id=uuid4())

    db_session.add(appointment)
    db_session.commit()

    appointment_id = appointment.id

    last_reattempt_at = datetime.utcnow()

    request = PendingAppointmentPaymentRequest(appointment_id=appointment_id, last_reattempt_at=last_reattempt_at)

    db_session.add(request)
    db_session.commit()

    appointments_service.reattempt_to_send_appointment_payment_request()

    request = db_session.query(PendingAppointmentPaymentRequest).get(appointment_id)

    assert request.reattempts == 0
    assert request.last_reattempt_at == last_reattempt_at


def test_not_reattempt_to_send_appointment_payment_request_if_appointment_not_found(appointments_service, db_session):
    appointment_id = uuid4()

    last_reattempt_at = datetime(2021, 1, 1)

    request = PendingAppointmentPaymentRequest(appointment_id=appointment_id, last_reattempt_at=last_reattempt_at)

    db_session.add(request)
    db_session.commit()

    appointments_service.reattempt_to_send_appointment_payment_request()

    assert db_session.query(PendingAppointmentPaymentRequest).get(appointment_id) is None
