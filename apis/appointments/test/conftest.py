import os

import pytest
from appointments.models import DeclarativeBase


@pytest.fixture(scope="session")
def db_url():
    user = os.getenv("APPOINTMENTS_API__POSTGRES_TEST_USER", "postgres")
    password = os.getenv("APPOINTMENTS_API__POSTGRES_TEST_PASSWORD", "password")
    host = os.getenv("APPOINTMENTS_API__POSTGRES_TEST_HOST", "localhost")
    port = os.getenv("APPOINTMENTS_API__POSTGRES_TEST_PORT", "5432")
    database = os.getenv("APPOINTMENTS_API__POSTGRES_TEST_DB", "appointments")

    return f"postgresql://{user}:{password}@{host}:{port}/{database}"


@pytest.fixture(scope="session")
def model_base():
    return DeclarativeBase
