from decimal import Decimal

from appointments.models import Appointment
from appointments.schemas import AppointmentSchema


def test_appointment_schema_dump():
    assert AppointmentSchema().dump(Appointment(price=Decimal(200))) == {
        "id": None,
        "start_date": None,
        "end_date": None,
        "price": 200,
        "patient_id": None,
        "physician_id": None,
    }


def test_appointment_schema_many_dump():
    assert AppointmentSchema(many=True).dump([Appointment(price=Decimal(200)), Appointment(price=Decimal(400))]) == [
        {
            "id": None,
            "start_date": None,
            "end_date": None,
            "price": 200,
            "patient_id": None,
            "physician_id": None,
        },
        {
            "id": None,
            "start_date": None,
            "end_date": None,
            "price": 400,
            "patient_id": None,
            "physician_id": None,
        },
    ]
