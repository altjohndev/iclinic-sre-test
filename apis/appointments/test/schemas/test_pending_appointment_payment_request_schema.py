from uuid import uuid4

from appointments.models import PendingAppointmentPaymentRequest
from appointments.schemas import PendingAppointmentPaymentRequestSchema


def test_appointment_schema_dump():
    appointment_id = str(uuid4())

    assert PendingAppointmentPaymentRequestSchema().dump(
        PendingAppointmentPaymentRequest(appointment_id=appointment_id)
    ) == {"appointment_id": appointment_id}


def test_appointment_schema_many_dump():
    appointment_id_1 = str(uuid4())
    appointment_id_2 = str(uuid4())

    assert (
        PendingAppointmentPaymentRequestSchema(many=True).dump(
            [
                PendingAppointmentPaymentRequest(appointment_id=appointment_id_1),
                PendingAppointmentPaymentRequest(appointment_id=appointment_id_2),
            ]
        )
        == [{"appointment_id": appointment_id_1}, {"appointment_id": appointment_id_2}]
    )
