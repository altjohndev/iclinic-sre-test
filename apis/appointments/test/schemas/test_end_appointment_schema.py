from datetime import datetime
from uuid import UUID, uuid4

import pytest
from appointments.schemas import EndAppointmentSchema
from marshmallow.exceptions import ValidationError
from marshmallow.utils import isoformat


def test_start_appointment_schema_load():
    result = EndAppointmentSchema().load({"id": str(uuid4()), "timestamp": datetime.utcnow().isoformat()})

    assert isinstance(result["id"], UUID)
    assert isinstance(result["timestamp"], datetime)


def test_start_appointment_schema_load_without_id():
    with pytest.raises(ValidationError) as exception_info:
        EndAppointmentSchema().load({"timestamp": datetime.utcnow().isoformat()})
    assert "'id'" in str(exception_info.value)
