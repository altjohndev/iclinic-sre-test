from datetime import datetime
from uuid import UUID, uuid4

import pytest
from appointments.schemas import StartAppointmentSchema
from marshmallow.exceptions import ValidationError
from marshmallow.utils import isoformat


def test_start_appointment_schema_load():
    result = StartAppointmentSchema().load(
        {"patient_id": str(uuid4()), "physician_id": str(uuid4()), "timestamp": datetime.utcnow().isoformat()}
    )

    assert isinstance(result["patient_id"], UUID)
    assert isinstance(result["physician_id"], UUID)
    assert isinstance(result["timestamp"], datetime)


def test_start_appointment_schema_load_without_patient_id():
    with pytest.raises(ValidationError) as exception_info:
        StartAppointmentSchema().load({"physician_id": str(uuid4()), "timestamp": datetime.utcnow().isoformat()})
    assert "'patient_id'" in str(exception_info.value)


def test_start_appointment_schema_load_without_physician_id():
    with pytest.raises(ValidationError) as exception_info:
        StartAppointmentSchema().load({"patient_id": str(uuid4()), "timestamp": datetime.utcnow().isoformat()})
    assert "'physician_id'" in str(exception_info.value)
