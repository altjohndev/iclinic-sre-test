from datetime import datetime
from decimal import Decimal
from uuid import UUID, uuid4

import pytest
from appointments.models import Appointment
from nameko_sqlalchemy import DatabaseSession
from sqlalchemy.exc import IntegrityError


def test_create_appointment(db_session: DatabaseSession):
    appointment = Appointment(patient_id=uuid4(), physician_id=uuid4())

    db_session.add(appointment)
    db_session.commit()

    assert isinstance(appointment.id, UUID)


def test_attempt_to_create_appointment_without_patient_id(db_session: DatabaseSession):
    appointment = Appointment(physician_id=uuid4())

    db_session.add(appointment)

    with pytest.raises(IntegrityError) as exception_info:
        db_session.commit()

    assert 'null value in column "patient_id"' in str(exception_info.value)


def test_attempt_to_create_appointment_without_physician_id(db_session: DatabaseSession):
    appointment = Appointment(patient_id=uuid4())

    db_session.add(appointment)

    with pytest.raises(IntegrityError) as exception_info:
        db_session.commit()

    assert 'null value in column "physician_id"' in str(exception_info.value)


def test_start_appointment_without_timestamp():
    appointment = Appointment()
    appointment.start_appointment()

    assert isinstance(appointment.start_date, datetime)


def test_start_appointment_with_string_timestamp():
    appointment = Appointment()
    appointment.start_appointment(datetime.utcnow().isoformat())

    assert isinstance(appointment.start_date, datetime)


def test_start_appointment_with_datetime_timestamp():
    appointment = Appointment()
    appointment.start_appointment(datetime.utcnow())

    assert isinstance(appointment.start_date, datetime)


def test_end_appointment_without_timestamp():
    appointment = Appointment()
    appointment.start_appointment()
    appointment.end_appointment()

    assert isinstance(appointment.end_date, datetime)
    assert isinstance(appointment.price, Decimal)


def test_end_appointment_with_string_timestamp():
    appointment = Appointment()
    appointment.start_appointment()
    appointment.end_appointment(datetime.utcnow().isoformat())

    assert isinstance(appointment.end_date, datetime)
    assert isinstance(appointment.price, Decimal)


def test_end_appointment_with_datetime_timestamp():
    appointment = Appointment()
    appointment.start_appointment()
    appointment.end_appointment(datetime.utcnow())

    assert isinstance(appointment.end_date, datetime)
    assert isinstance(appointment.price, Decimal)


def test_end_appointment_without_starting_it_before():
    appointment = Appointment()
    appointment.end_appointment()

    assert appointment.end_date is None
    assert appointment.price is None


def test_update_price_with_one_hour_appointment():
    appointment = Appointment(start_date=datetime(2021, 12, 1, 13), end_date=datetime(2021, 12, 1, 14))
    appointment.update_price()

    assert appointment.price == Decimal(200)


def test_update_price_with_one_hour_and_some_minutes_appointment():
    appointment = Appointment(start_date=datetime(2021, 12, 1, 13), end_date=datetime(2021, 12, 1, 14, 15))
    appointment.update_price()

    assert appointment.price == Decimal(400)


def test_update_price_without_start_date():
    appointment = Appointment(end_date=datetime(2021, 12, 1, 14))
    appointment.update_price()

    assert appointment.price is None


def test_update_price_without_end_date():
    appointment = Appointment(start_date=datetime(2021, 12, 1, 13))
    appointment.update_price()

    assert appointment.price is None
