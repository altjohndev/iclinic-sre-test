from uuid import UUID, uuid4

import pytest
from appointments.models import PendingAppointmentPaymentRequest
from nameko_sqlalchemy import DatabaseSession
from sqlalchemy.exc import IntegrityError, SAWarning


def test_create_pending_appointment_payment_request(db_session: DatabaseSession):
    request = PendingAppointmentPaymentRequest(appointment_id=uuid4())

    db_session.add(request)
    db_session.commit()

    assert isinstance(request.appointment_id, UUID)


def test_attempt_to_create_appointment_without_appointment_id(db_session: DatabaseSession):
    request = PendingAppointmentPaymentRequest()

    db_session.add(request)

    with pytest.raises(IntegrityError) as exception_info:
        with pytest.warns(SAWarning):
            db_session.commit()

    assert 'null value in column "appointment_id"' in str(exception_info.value)
