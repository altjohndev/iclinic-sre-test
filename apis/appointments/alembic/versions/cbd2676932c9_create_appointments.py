"""create appointments

Revision ID: cbd2676932c9
Revises:
Create Date: 2021-06-10 12:46:32.680252

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import UUID

# revision identifiers, used by Alembic.
revision = "cbd2676932c9"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "appointments",
        sa.Column("id", UUID(), nullable=False),
        sa.Column("start_date", sa.DateTime()),
        sa.Column("end_date", sa.DateTime()),
        sa.Column("price", sa.DECIMAL(18, 2)),
        sa.Column("patient_id", UUID(), nullable=False),
        sa.Column("physician_id", UUID(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )

    op.create_table(
        "pending_appointments_payment_requests",
        sa.Column("appointment_id", UUID(), nullable=False),
        sa.Column("reattempts", sa.Integer(), nullable=False, default=0),
        sa.Column("last_reattempt_at", sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint("appointment_id"),
    )


def downgrade():
    op.drop_table("pending_appointments_payment_requests")
    op.drop_table("appointments")
