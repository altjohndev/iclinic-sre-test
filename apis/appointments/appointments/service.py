from datetime import datetime
from typing import Callable

from marshmallow.exceptions import ValidationError
from nameko.events import EventDispatcher, event_handler
from nameko.rpc import rpc
from nameko.timer import timer
from nameko_sqlalchemy import DatabaseSession

from appointments.exceptions import AppointmentNotFound
from appointments.models import Appointment, DeclarativeBase, PendingAppointmentPaymentRequest
from appointments.schemas import (
    AppointmentSchema,
    EndAppointmentSchema,
    PendingAppointmentPaymentRequestSchema,
    StartAppointmentSchema,
)
from appointments.wrappers import safe_return


class AppointmentsService:
    """
    Service responsible to manage appointments data.

    Methods
    -------
    list_appointments()
        Lists all registered appointments
    list_pending_appointments_payment_requests()
        Lists all registered pending appointments payment requests
    start_appointment(data)
        Registers a new appointment with a start_date
    end_appointment(data)
        Updates an existing appointment with an end_date and price.
        Also, broadcasts that the appointment finished.
        Also, creates a backup for the appointment payment request
    handle_appointment_payment_request_received(payload)
        Removes the pending appointment payment request.
    reattempt_to_send_appointment_payment_request()
        Reabroadcasts each appointment not received by payment API.
        The rebroadcast occurs if the last reattempt was performed later than the threshold.
        The threshold is the amount of reattempts performed multiplied by 30 seconds, for a maximum of 5 minutes
    """

    name = "appointments"

    db = DatabaseSession(DeclarativeBase)
    event_dispatcher = EventDispatcher()

    @rpc
    @safe_return
    def list_appointments(self) -> dict:
        appointments = self.db.query(Appointment).all()
        return AppointmentSchema(many=True).dump(appointments)

    @rpc
    @safe_return
    def list_pending_appointments_payment_requests(self) -> dict:
        pending_requests = self.db.query(PendingAppointmentPaymentRequest).all()
        return PendingAppointmentPaymentRequestSchema(many=True).dump(pending_requests)

    @rpc
    @safe_return
    def start_appointment(self, data: dict) -> dict:
        data = StartAppointmentSchema().load(data)

        appointment = Appointment(patient_id=data["patient_id"], physician_id=data["physician_id"])
        appointment.start_appointment(data.get("timestamp"))

        self.db.add(appointment)
        self.db.commit()

        print(f"[{datetime.utcnow()}] Appointment started. ID: {appointment.id}")

        appointment = AppointmentSchema().dump(appointment)

        return appointment

    @rpc
    @safe_return
    def end_appointment(self, data: dict) -> dict:
        data = EndAppointmentSchema().load(data)

        appointment_id = data["id"]
        appointment = self.db.query(Appointment).get(appointment_id)

        if not appointment:
            raise AppointmentNotFound(appointment_id)

        appointment.end_appointment(data.get("timestamp"))

        pending_appointment_payment_request = PendingAppointmentPaymentRequest(appointment_id=appointment.id)

        self.db.add(pending_appointment_payment_request)
        self.db.commit()

        print(f"[{datetime.utcnow()}] Appointment finished. ID: {appointment.id}")

        appointment = AppointmentSchema().dump(appointment)

        self.event_dispatcher("appointment_finished", appointment)

        return appointment

    @event_handler("payments", "appointment_payment_request_received")
    def handle_appointment_payment_request_received(self, payload: dict) -> None:
        appointment_id = payload["appointment_id"]
        pending_request = self.db.query(PendingAppointmentPaymentRequest).get(appointment_id)

        if pending_request is not None:
            self.db.delete(pending_request)
            self.db.commit()

            print(f"[{datetime.utcnow()}] Pending appointment payment request removed. ID: {appointment_id}")

    @timer(interval=30)
    def reattempt_to_send_appointment_payment_request(self) -> None:
        now = datetime.utcnow()
        amount_to_remove = 0

        for pending_request in self.db.query(PendingAppointmentPaymentRequest).all():
            if (now - pending_request.last_reattempt_at).total_seconds() > min(pending_request.reattempts + 1, 10) * 30:

                appointment = self.db.query(Appointment).get(pending_request.appointment_id)

                if appointment:
                    appointment = AppointmentSchema().dump(appointment)

                    self.event_dispatcher("appointment_finished", appointment)

                    pending_request.reattempts += 1
                    pending_request.last_reattempt_at = now

                    print(f"[{now}] Reattempted to request appointment payment. ID: {pending_request.appointment_id}")
                else:
                    self.db.delete(pending_request)
                    amount_to_remove += 1

        if amount_to_remove > 0:
            self.db.commit()
