import math
import uuid
from datetime import datetime
from decimal import Decimal
from typing import Optional

from sqlalchemy import DECIMAL, Column, DateTime, Integer
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import declarative_base

DeclarativeBase = declarative_base()


class Appointment(DeclarativeBase):
    """
    Model used to represent an arrangement to meet a physician with a patient.

    Attributes
    ----------
    id : uuid
        the primary key of the model
    start_date : datetime
        the datetime from which the appointment started
    end_date : datetime
        the datetime from which the appointment finished
    price : Decimal
        the price for the appointment.
        Defined if the appointment finished.
        It is defined as the total hours (rounded up) of the appointment multiplied by 200.
        The currency is BRL (Brazilian real)
    patient_id : uuid
        the patient identifier
    physician_id : uuid
        the physician identifier

    Methods
    -------
    start_appointment(timestamp=None)
        updates the start_date attribute.
        If timestamp is None, datetime.utcnow will be used
    end_appointment(timestamp=None)
        updates the end_date and price attributes.
        If timestamp is None, datetime.utcnow will be used
    update_price()
        updates price attribute based on start_date and end_date
    """

    __tablename__ = "appointments"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)

    start_date = Column(DateTime)
    end_date = Column(DateTime)

    price = Column(DECIMAL(18, 2))

    patient_id = Column(UUID(as_uuid=True), nullable=False)
    physician_id = Column(UUID(as_uuid=True), nullable=False)

    def start_appointment(self, timestamp: Optional[datetime] = None) -> "Appointment":
        if not timestamp:
            timestamp = datetime.utcnow()

        if isinstance(timestamp, str):
            timestamp = datetime.fromisoformat(timestamp)

        self.start_date = timestamp

        return self

    def end_appointment(self, timestamp: Optional[datetime] = None) -> "Appointment":
        if not timestamp:
            timestamp = datetime.utcnow()

        if isinstance(timestamp, str):
            timestamp = datetime.fromisoformat(timestamp)

        self.end_date = timestamp

        self.update_price()

        if self.price is None:
            self.end_date = None

        return self

    def update_price(self) -> "Appointment":
        if self.start_date and self.end_date:
            delta = self.end_date - self.start_date
            hours = math.ceil(delta.total_seconds() / 3600)
            self.price = Decimal(hours * 200)
        else:
            self.price = None

        return self


class PendingAppointmentPaymentRequest(DeclarativeBase):
    """
    Model used as a cache for appointments not handled by the payments API.

    Attributes
    ----------
    appointment_id : uuid
        the appointment identifier and the primary key of this model
    reattempts : integer
        number of attempts performed
    last_reattempt_at : datetime
        the datetime of the most recent reattempt performed
    """

    __tablename__ = "pending_appointments_payment_requests"

    appointment_id = Column(UUID(as_uuid=True), primary_key=True, nullable=False)

    reattempts = Column(Integer, nullable=False, default=0)
    last_reattempt_at = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow, nullable=False)
