from marshmallow import Schema, fields


class AppointmentSchema(Schema):
    """
    Schema used to represent an arrangement to meet a physician with a patient.

    Attributes
    ----------
    id : uuid
        the appointment identifier
    start_date : datetime
        the datetime from which the appointment started
    end_date : datetime
        the datetime from which the appointment finished
    price : Decimal
        the price for the appointment
    patient_id : uuid
        the patient identifier
    physician_id : uuid
        the physician identifier
    """

    id = fields.UUID(required=True)
    start_date = fields.DateTime()
    end_date = fields.DateTime()
    price = fields.Decimal()
    patient_id = fields.UUID(required=True)
    physician_id = fields.UUID(required=True)


class StartAppointmentSchema(Schema):
    """
    Schema to represent the payload used to start an appointment.

    Attributes
    ----------
    timestamp : datetime
        the datetime from which the appointment started
    patient_id : uuid
        the patient identifier
    physician_id : uuid
        the physician identifier
    """

    timestamp = fields.DateTime()

    patient_id = fields.UUID(required=True)
    physician_id = fields.UUID(required=True)


class EndAppointmentSchema(Schema):
    """
    Schema to represent the payload used to end an appointment.

    Attributes
    ----------
    id : uuid
        the appointment identifier
    timestamp : datetime
        the datetime from which the appointment finished
    """

    id = fields.UUID(required=True)
    timestamp = fields.DateTime()


class PendingAppointmentPaymentRequestSchema(Schema):
    """
    Schema used as a cache for appointments not handled by the payments API.

    Attributes
    ----------
    appointment_id : uuid
        the appointment identifier
    """

    appointment_id = fields.UUID(required=True)
