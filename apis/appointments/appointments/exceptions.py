class AppointmentNotFound(Exception):
    """Raised when appointment_id does not represent any appointment in database."""

    def __init__(self, appointment_id: str) -> None:
        self.message = f'Appointment not found. ID: "{appointment_id}"'
