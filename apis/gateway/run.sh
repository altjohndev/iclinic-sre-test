#!/bin/bash

until nc -z ${GATEWAY_API__RABBITMQ_HOST:-localhost} ${GATEWAY_API__RABBITMQ_PORT:-5672}; do
  echo "$(date) - waiting for rabbitmq..."
  sleep 2
done

nameko run --config nameko.yml gateway.service --backdoor 3000
