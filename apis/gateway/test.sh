#!/bin/bash

echo "# black code style check..."

black --check .

echo "# pycodestyle code style check..."

pycodestyle .

echo "# running tests..."

coverage run --source=gateway -m pytest

echo "# coverage results"

coverage report --show-missing
