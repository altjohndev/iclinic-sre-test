# iClinic SRE Test - Gateway Service

Service responsible to expose HTTP API from internal microservices operations.

## Technologies

- [Python](https://www.python.org/) - An interpreted high-level general-purpose programming language.

- [Nameko](https://nameko.readthedocs.io/en/stable/) - A microservices framework for Python that lets service
  developers concentrate on application logic and encourages testability.

- [Marshmallow](https://marshmallow.readthedocs.io/en/stable/) - An ORM/ODM/framework-agnostic library for converting
  complex datatypes, such as objects, to and from native Python datatypes.

## Usage with Docker

You can set up the service using [docker-compose](https://docs.docker.com/compose/):

```yaml
version: '3.8'

services:
  gateway:
    image: arkye/iclinic-sre-test:gateway
    environment:
      PHYSICIANS_API__RABBITMQ_HOST: rabbitmq
      PHYSICIANS_API__POSTGRES_HOST: postgres
    networks:
      - network
    depends_on:
      - rabbitmq

  rabbitmq:
    image: rabbitmq:3.8.17-management
    ports:
      - 5672:5672
      - 15672:15672
    networks:
      - network

networks:
  network:
    name: iclinic_sre_test
```

To start the services:

```bash
docker-compose up -d
```

To stop the services:

```bash
docker-compose down
```

## Endpoints

- `POST /patients` - Registers a new patient

  - Payload:

    - `name` (Required) - The name of the patient

  - Response data:

    - `id` - The patient identifier
    - `name` - The name of the patient

- `POST /physicians` - Registers a new physician

  - Payload:

    - `name` (Required) - The name of the physician

  - Response data:

    - `id` - The physician identifier
    - `name` - The name of the physician

- `POST /appointments/start` - Registers a new appointment

  - Payload:

    - `patient_id` (Required) - The patient identifier
    - `physician_id` (Required) - The physician identifier
    - `timestamp` - The datetime from which the appointment started. If not defined, the current datetime will be used

  - Response data:

    - `id` - The appointment identifier
    - `start_date` - The datetime from which the appointment started
    - `end_date` - The datetime from which the appointment finished
    - `price` - the price for the appointment
    - `patient_id` - The patient identifier
    - `physician_id` - The physician identifier

- `GET /appointments/<appointment_id>/end` - End an existing started appointment, retrieving the updated appointment

  - Response data:

    - `id` - The appointment identifier
    - `start_date` - The datetime from which the appointment started
    - `end_date` - The datetime from which the appointment finished
    - `price` - the price for the appointment
    - `patient_id` - The patient identifier
    - `physician_id` - The physician identifier

- `GET /appointments/<appointment_id>/payment_request` - Retrieve the payment request for the appointment

  - Response data:

    - `appointment_id` - The appointment identifier
    - `total_price` - the price for the appointment

## Environment Variables

### Server

- `GATEWAY_API__SERVER_ADDRESS` - The server address. Defaults to `0.0.0.0`
- `GATEWAY_API__SERVER_PORT` - The server port. Defaults to `8000`

### RabbitMQ

- `PATIENTS_API__RABBITMQ_USER` - The RabbitMQ user. Defaults to `guest`
- `PATIENTS_API__RABBITMQ_PASSWORD` - The RabbitMQ user password. Defaults to `guest`
- `PATIENTS_API__RABBITMQ_HOST` - The RabbitMQ hostname. Defaults to `localhost`
- `PATIENTS_API__RABBITMQ_PORT` - The RabbitMQ port. Defaults to `5672`

## Development

### Local setup

To install dependencies, use:

```bash
pip install -e .[dev]
```

### Tests

Simply execute the `test.sh` file.
