#!/usr/bin/env python

from setuptools import find_packages, setup

REQUIREMENTS = (
    "marshmallow==3.12.1",
    "nameko==2.13.0",
    "simplejson==3.17.2",
)

DEV_REQUIREMENTS = (
    "autoflake==1.4",
    "black==21.5b2",
    "coverage==5.5",
    "pycodestyle==2.7.0",
    "pylint==2.8.3",
    "pytest==6.2.4",
)

setup(
    name="iClinic SRE Test - Gateway Service",
    version="0.0.1",
    packages=find_packages(),
    python_requires=">=3.9",
    install_requires=REQUIREMENTS,
    extras_require={"dev": DEV_REQUIREMENTS},
    zip_safe=True,
)
