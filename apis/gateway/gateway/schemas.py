from marshmallow import Schema, fields


class AppointmentSchema(Schema):
    """
    Schema used to represent an arrangement to meet a physician with a patient.

    Attributes
    ----------
    id : uuid
        the appointment identifier
    start_date : datetime
        the datetime from which the appointment started
    end_date : datetime
        the datetime from which the appointment finished
    price : Decimal
        the price for the appointment
    patient_id : uuid
        the patient identifier
    physician_id : uuid
        the physician identifier
    """

    id = fields.UUID(required=True)
    start_date = fields.DateTime(allow_none=True)
    end_date = fields.DateTime(allow_none=True)
    price = fields.Decimal(allow_none=True)
    patient_id = fields.UUID(required=True)
    physician_id = fields.UUID(required=True)


class StartAppointmentSchema(Schema):
    """
    Schema to represent the payload used to start an appointment.

    Attributes
    ----------
    timestamp : datetime
        the datetime from which the appointment started
    patient_id : uuid
        the patient identifier
    physician_id : uuid
        the physician identifier
    """

    timestamp = fields.DateTime()
    patient_id = fields.UUID(required=True)
    physician_id = fields.UUID(required=True)


class EndAppointmentSchema(Schema):
    """
    Schema to represent the payload used to end an appointment.

    Attributes
    ----------
    timestamp : datetime
        the datetime from which the appointment finished
    """

    timestamp = fields.DateTime(allow_none=True)


class AppointmentPaymentRequestSchema(Schema):
    """
    Schema used to represent a payment request for a finished appointment between patient and physician.

    Attributes
    ----------
    appointment_id : uuid
        the appointment identifier
    total_price : Decimal
        the price for the appointment
    """

    appointment_id = fields.UUID(required=True)
    total_price = fields.Decimal(allow_none=True)


class PatientSchema(Schema):
    """
    Schema used to represent a person receiving or registered to receive medical treatment.

    Attributes
    ----------
    id : uuid
        the identifier of the schema
    name : str
        the name of the patient
    """

    id = fields.UUID(required=True)
    name = fields.String(required=True)


class CreatePatientSchema(Schema):
    """
    Schema to represent the payload used to create a patient.

    Attributes
    ----------
    name : str
        the name of the patient
    """

    name = fields.String(required=True)


class PhysicianSchema(Schema):
    """
    Schema used to represent a person qualified to practice medicine.

    Attributes
    ----------
    id : uuid
        the identifier of the schema
    name : str
        the name of the physician
    """

    id = fields.UUID(required=True)
    name = fields.String(required=True)


class CreatePhysicianSchema(Schema):
    """
    Schema to represent the payload used to create a physician.

    Attributes
    ----------
    name : str
        the name of the physician
    """

    name = fields.String(required=True)
