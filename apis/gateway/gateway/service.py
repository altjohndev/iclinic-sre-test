from decimal import Decimal

from nameko.rpc import RpcProxy
from nameko.web.handlers import http

from gateway.decorators import safe_response
from gateway.schemas import (
    AppointmentPaymentRequestSchema,
    AppointmentSchema,
    CreatePatientSchema,
    CreatePhysicianSchema,
    EndAppointmentSchema,
    PatientSchema,
    PhysicianSchema,
    StartAppointmentSchema,
)


class GatewayService:
    """
    Service responsible to expose HTTP API from internal microservices operations.

    Methods
    -------
    create_patient(request)
        Registers a new patient
    create_physician(request)
        Registers a new physician
    start_appointment(request)
        Registers a new appointment with a start_date
    end_appointment(request, appointment_id)
        Updates an existing appointment with an end_date and price.
    get_appointment_payment_request(request, appointment_id)
        Retrieves a registered appointment payment request based on appointment_id
    """

    name = "gateway"

    appointments_rpc = RpcProxy("appointments")
    patients_rpc = RpcProxy("patients")
    physicians_rpc = RpcProxy("physicians")
    payments_rpc = RpcProxy("payments")

    @http("POST", "/patients")
    @safe_response(status_code=201, schema=PatientSchema)
    def create_patient(self, request):
        payload = request.get_data(as_text=True)
        patient_data = CreatePatientSchema().loads(payload)

        return self.patients_rpc.create_patient(patient_data)

    @http("POST", "/physicians")
    @safe_response(status_code=201, schema=PhysicianSchema)
    def create_physician(self, request):
        payload = request.get_data(as_text=True)
        physician_data = CreatePhysicianSchema().loads(payload)

        return self.physicians_rpc.create_physician(physician_data)

    @http("POST", "/appointments/start")
    @safe_response(status_code=201, schema=AppointmentSchema)
    def start_appointment(self, request):
        payload = request.get_data(as_text=True)
        appointment_data = StartAppointmentSchema().loads(payload)

        return self.__to_decimal(self.appointments_rpc.start_appointment(appointment_data), "price")

    @http("POST", "/appointments/<string:appointment_id>/end")
    @safe_response(schema=AppointmentSchema)
    def end_appointment(self, request, appointment_id):
        appointment_data = EndAppointmentSchema().loads(request.get_data(as_text=True))
        appointment_data["id"] = appointment_id

        return self.__to_decimal(self.appointments_rpc.end_appointment(appointment_data), "price")

    @http("GET", "/appointments/<string:appointment_id>/payment_request")
    @safe_response(schema=AppointmentPaymentRequestSchema)
    def get_appointment_payment_request(self, request, appointment_id):
        return self.__to_decimal(self.payments_rpc.get_appointment_payment_request(appointment_id), "total_price")

    def __to_decimal(self, data, key):
        """Function required in order to force Decimal values as floats when dumping to JSON"""

        if "data" in data:
            value = data["data"].get(key)
            if value:
                data["data"][key] = Decimal(value)

        return data
