import functools
from datetime import datetime
from logging import error
from typing import Callable

import simplejson
from marshmallow import Schema
from marshmallow.exceptions import ValidationError
from werkzeug import Response

MIMETYPE = "application/json"


def safe_response(status_code: int = 200, schema: Schema = None) -> Callable:
    """Decorates a function to handle data, returning a response. Also handles exceptions."""

    def decorator(function):
        @functools.wraps(function)
        def wrapper(*args, **kwargs) -> Callable:
            try:
                result = function(*args, **kwargs)

                if result["has_errors"] is False:
                    data = result["data"]
                    if schema:
                        data = schema().load(data)
                        data = schema().dump(data)
                    return Response(simplejson.dumps(data), status=status_code, mimetype=MIMETYPE)

                error_type = result["error_type"]

                if "NotFound" in error_type:
                    return Response(simplejson.dumps(result["error_data"]), status=404, mimetype=MIMETYPE)
                elif error_type == "ValidationError":
                    return Response(simplejson.dumps(result["error_data"]), status=400, mimetype=MIMETYPE)

                return Response(simplejson.dumps(result["error_data"]), status=500, mimetype=MIMETYPE)

            except ValidationError as exception:
                return Response(simplejson.dumps(exception.normalized_messages()), status=400, mimetype=MIMETYPE)
            except Exception as exception:
                print(
                    f"[{datetime.utcnow()}] Failed to handle request: {exception}. "
                    f"function: {function}, args: {args}, kwargs: {kwargs}"
                )

                return Response(simplejson.dumps({"message": "Internal server error"}), status=500, mimetype=MIMETYPE)

        return wrapper

    return decorator
