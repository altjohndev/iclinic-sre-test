from uuid import uuid4

import pytest
import simplejson
from gateway.service import GatewayService
from nameko.testing.services import worker_factory


def valid_data(data) -> dict:
    return {"has_errors": False, "data": data}


@pytest.fixture
def gateway_service():
    gateway_service = worker_factory(GatewayService)

    gateway_service.patients_rpc.create_patient.side_effect = lambda d: valid_data({"id": uuid4(), "name": "John Doe"})
    gateway_service.physicians_rpc.create_physician.side_effect = lambda d: valid_data(
        {"id": uuid4(), "name": "Joana Doe"}
    )
    gateway_service.appointments_rpc.start_appointment.side_effect = lambda d: valid_data(
        {"id": uuid4(), "patient_id": uuid4(), "physician_id": uuid4()}
    )
    gateway_service.appointments_rpc.end_appointment.side_effect = lambda d: valid_data(
        {"id": uuid4(), "patient_id": uuid4(), "physician_id": uuid4()}
    )
    gateway_service.payments_rpc.get_appointment_payment_request.side_effect = lambda d: valid_data(
        {"appointment_id": uuid4(), "total_price": "200.00"}
    )

    return gateway_service


@pytest.fixture
def mocked_request():
    class Request:
        data = {}

        def get_data(self, as_text=None):
            return simplejson.dumps(self.data)

        def set_data(self, data):
            self.data = data

            return self

    return Request()
