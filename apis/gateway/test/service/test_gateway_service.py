from uuid import uuid4


def test_create_patient(gateway_service, mocked_request):
    mocked_request.set_data({"name": "John Doe"})
    response = gateway_service.create_patient(mocked_request)

    assert response.status_code == 201


def test_attempt_to_create_patient_with_invalid_data(gateway_service, mocked_request):
    mocked_request.set_data({"name": None})
    response = gateway_service.create_patient(mocked_request)

    assert response.status_code == 400


def test_create_physician(gateway_service, mocked_request):
    mocked_request.set_data({"name": "John Doe"})
    response = gateway_service.create_physician(mocked_request)

    assert response.status_code == 201


def test_attempt_to_create_physician_without_name(gateway_service, mocked_request):
    mocked_request.set_data({})
    response = gateway_service.create_physician(mocked_request)

    assert response.status_code == 400


def test_start_appointment(gateway_service, mocked_request):
    mocked_request.set_data({"patient_id": str(uuid4()), "physician_id": str(uuid4())})
    response = gateway_service.start_appointment(mocked_request)

    assert response.status_code == 201


def test_attempt_to_start_appointment_without_patient_id(gateway_service, mocked_request):
    mocked_request.set_data({"patient_id": str(uuid4())})
    response = gateway_service.start_appointment(mocked_request)

    assert response.status_code == 400


def test_attempt_to_start_appointment_without_physician_id(gateway_service, mocked_request):
    mocked_request.set_data({"physician_id": str(uuid4())})
    response = gateway_service.start_appointment(mocked_request)

    assert response.status_code == 400


def test_end_appointment(gateway_service, mocked_request):
    response = gateway_service.end_appointment(mocked_request, appointment_id=str(uuid4()))

    assert response.status_code == 200


def test_get_appointment_payment_request(gateway_service, mocked_request):
    response = gateway_service.get_appointment_payment_request(mocked_request, appointment_id=str(uuid4))

    assert response.status_code == 200
