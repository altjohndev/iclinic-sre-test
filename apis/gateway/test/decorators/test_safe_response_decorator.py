from uuid import uuid4

import simplejson
from gateway.decorators import safe_response
from gateway.schemas import PatientSchema


def test_wrap_valid_data_with_schema():
    data = {"id": uuid4(), "name": "John Doe"}

    @safe_response(schema=PatientSchema)
    def function():
        return {"has_errors": False, "data": data}

    assert simplejson.loads(function().response[0]) == PatientSchema().dump(data)


def test_set_response_with_status_code():
    @safe_response(status_code=501)
    def function():
        return {"has_errors": False, "data": {}}

    assert function().status_code == 501


def test_wrap_not_found_invalid_data():
    data = {"message": "Something not found."}

    @safe_response()
    def function():
        return {"has_errors": True, "error_type": "SomethingNotFound", "error_data": data}

    result = function()
    assert result.status_code == 404
    assert simplejson.loads(function().response[0]) == data


def test_wrap_validation_error_invalid_data():
    data = {"name": "Required."}

    @safe_response()
    def function():
        return {"has_errors": True, "error_type": "ValidationError", "error_data": data}

    result = function()
    assert result.status_code == 400
    assert simplejson.loads(function().response[0]) == data


def test_wrap_unhandled_invalid_data():
    data = {"message": "?"}

    @safe_response()
    def function():
        return {"has_errors": True, "error_type": "UnknownError", "error_data": data}

    result = function()
    assert result.status_code == 500
    assert simplejson.loads(function().response[0]) == data


def test_wrap_on_service_error():
    @safe_response()
    def function():
        raise Exception("Boo boo")

    result = function()
    assert result.status_code == 500
    assert simplejson.loads(function().response[0]) == {"message": "Internal server error"}
