#!/bin/bash

echo "# black code style check..."

black --check .

echo "# pycodestyle code style check..."

pycodestyle .

echo "# running tests..."

(cd apis/appointments && coverage run --source=appointments -m pytest)
(cd apis/gateway && coverage run --source=gateway -m pytest)
(cd apis/patients && coverage run --source=patients -m pytest)
(cd apis/payments && coverage run --source=payments -m pytest)
(cd apis/physicians && coverage run --source=physicians -m pytest)

echo "# coverage results"

coverage combine apis/*/.coverage
coverage report --show-missing
