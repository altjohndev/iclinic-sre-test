# iClinic SRE Test

Este projeto busca solucionar o [desafio iClinic Teste SRE Arquiteto](.misc/docs/challenge.md).

## Sumário

- [Análise do desafio](.misc/docs/challenge_analysis.md)
- [Instalação e Configuração](.misc/docs/installation.md)
- [Roteiro de aplicação](.misc/docs/cookbook.md)

## Serviços

- [Appointments](apis/appointments)
- [Gateway](apis/gateway)
- [Patients](apis/patients)
- [Payments](apis/payments)
- [Physicians](apis/physicians)
