# Análise do desafio

- 2 Microsserviços, 1 para consultas, 1 para financeiro. Ver [microsserviços](#microsserviços)
- Desenvolvido em Python
- Docker para provisionar serviços. Ver [provisionamento](#provisionamento)
- Podem utilizar um banco de dados compartilhado. Ver [banco de dados](#banco-de-dados)
- Boas práticas e padrões de código. Ver [boas práticas](#boas-práticas)
- Pontos de falhas. Ver [limitações](#limitações)
- Testes unitários. Ver [testes](#testes)
- Facilidade em levantar o ambiente. Ver [provisionamento](#provisionamento)

## Microsserviços

O desafio espera a construção de dois microsserviços:

- **API de consultas** - Para iniciar e finalizar consultas, com a seguinte representação:

  ```json
  {
    "id": "84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
    "start_date": "2020-12-01 13:00:00",
    "end_date": "2020-12-01 14:00:00",
    "physician_id": "ea959b03-5577-45c9-b9f7-a45d3e77ce82",
    "patient_id": "86158d46-ce33-4e3d-9822-462bbff5782e",
    "price": 200.00
  }
  ```

- **API financeira** - Para receber entradas financeiras de cobrança para consultas.

  ```json
  {
    "appointment_id": "84ab6121-c4a8-4684-8cc2-b03024ec0f1d",
    "total_price": 400.00,
  }
  ```

Para cumprir o desafio:

- A "API de consultas" foi definida no serviço [appointments](../../apis/appointments/README.md);
- A "API financeira" foi definida no serviço [payments](../../apis/payments/README.md);
- O serviço [gateway](../../apis/gateway/README.md) disponibiliza os endpoints para iniciar consultas, encerrar consultas e visualizar entradas financeiras;

Adicionalmente, os serviços [patients](../../apis/patients/README.md) e [physicians](../../apis/physicians/README.md) foram criados com o objetivo de criar pacientes e médicos no `gateway`.

A regras de negócio para definição do valor de consultas ocorre no microsserviço `appointments`.

A tecnologia escolhida foi [Nameko](https://nameko.readthedocs.io/en/stable/), biblioteca em Python para criação de microsserviços.
Tal tecnologia fornece uma interface no Python para comunicação RPC entre serviços, eventos e suas operações. Tal interface simplifica a interação entre microsserviços, traduzindo mensagens em um broker RabbitMQ.

### Notificação de cobrança

A notificação de cobrança para o serviço financeiro é realizada através do disparo de um evento em `appointments`, este que dispara uma mensagem para o _exchange_ `appointments.events`, monitorado por `payments`. Adicionalmente, `appointments` salva no banco um backup da notificação.

Caso exista um microsserviço `payments` disponível, este irá receber a cobrança e notificará o sucesso da operação no _exchange_ `payments.events`, monitorado por `appointments`.

Caso exista um microsserviço `appointments` disponível, este irá receber a notificação de sucesso e excluirá o backup da notificação no banco.

Caso `appointments` não receba a notificação de sucesso, este irá repetir a notificação de cobrança, de acordo com as seguintes regras:

- Após 30 segundos de ocorrência da primeira notificação sem respostas de sucesso, uma segunda notificação é enviada;
- Após 60 segundos de ocorrência da segunda notificação sem respostas de sucesso, uma terceira notificação é enviada;
- O tempo de espera é definido como `30` segundos multiplicado pela quantidade de tentativas, com um limite máximo de `300` segundos (`5` minutos).

## Provisionamento

Para facilitar o provisionamento, imagens Docker para cada serviço estão disponíveis em [arkye/iclinic-sre-test](https://hub.docker.com/r/arkye/iclinic-sre-test). É possível configurar cada serviço utilizando de variáveis de ambiente.

Para maiores detalhes, ver [Instalação e Configuração](installation.md).

## Banco de dados

Serviços podem configurar, individualmente, a comunicação com o banco de dados PostgreSQL.

Com isto, é possível configurar para todos os serviços utilizarem o mesmo banco ou bancos distintos, de acordo com a necessidade.

Para maiores detalhes, ver [Instalação e Configuração](installation.md).

## Boas práticas

Para assegurar boas práticas e padrões de código, foi utilizado:

- De técnicas de OO e recursos em Python com o objetivo de aperfeiçoar o código, suas operações e seus testes;
- De tecnologias para validação de estrutura do código: [pycodestyle](https://pycodestyle.pycqa.org/en/latest/), [black](https://black.readthedocs.io/en/stable/), [flake](https://flake8.pycqa.org/en/latest/) e [pylint](http://pylint.pycqa.org/en/latest/);
- De documentação em nível de classe e métodos (quando necessário).
- De tipagem em definição de funções e métodos.

## Limitações

Principais limitações do projeto:

- Identificadores NÃO são validados entre contextos de diferentes serviços.
  Para um projeto real, se faz necessário assegurar a existência de elementos entre os serviços.
  Há várias soluções (com suas respectivas limitações), tais como:

  - Responsabilizar `gateway` para coletar ou validar os elementos entre os serviços antes de requisitar uma operação;
  - Responsabilizar cada serviço para coletar ou validar os elementos necessários para sua operação;
  - Estabelecer um comportamento de broadcasting e caching em cada serviço.

- Cada microsserviço (exceto `gateway`) permite sua comunicação apenas via AMQP, utilizando de um broker RabbitMQ.
  Caso necessário, é possível gerar novas interfaces de comunicação.

- `gateway` utiliza de Nameko para disponibilizar a API HTTP do projeto. Tal biblioteca é limitada, e em caso de uso em produção, é preferível migrar para um _framework_ mais robusto, como o [Flask](https://flask.palletsprojects.com/en/2.0.x/);

- Não há autenticação, nem definição de sessão de usuário.

## Testes

É realizado validação de estrutura de código utilizando `black` e `pycodestyle`.

Há testes unitários em todos os serviços, utilizando de [pytest](https://docs.pytest.org/en/6.2.x/) e [coverage](https://coverage.readthedocs.io/en/coverage-5.5/). Totalizando 100% de cobertura em cada serviço, conforme [registro](test_log.txt).
