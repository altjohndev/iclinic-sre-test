# Roteiro de aplicação

## Crie um paciente

```bash
curl \
  --header "Content-Type: application/json" \
  --request POST \
  --data '{"name":"John Doe"}' \
  http://localhost:8000/patients
```

Exemplo de resultado:

```json
{
  "id": "30774e9f-c105-4868-b83c-fbddd297c4b1",
  "name": "John Doe"
}
```

Comportamento:

- `gateway` recebe requisição (HTTP);
- `gateway` requisita criação para `patients` via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- `patients` recebe mensagem de requisição;
- `patients` cria paciente;
- `patients` retorna paciente via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- `gateway` recebe paciente;
- `gateway` transmite paciente como resposta (HTTP).

## Crie um médico

```bash
curl \
  --header "Content-Type: application/json" \
  --request POST \
  --data '{"name":"Joana Doe"}' \
  http://localhost:8000/physicians
```

Exemplo de resultado:

```json
{
  "id": "35d3aaab-edfd-4c47-8117-4a3d53417e7e",
  "name": "Joana Doe"
}
```

Comportamento:

- `gateway` recebe requisição (HTTP);
- `gateway` requisita criação para `physicians` via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- `physicians` recebe mensagem de requisição;
- `physicians` cria médico;
- `physicians` retorna médico via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- `gateway` recebe médico;
- `gateway` transmite médico como resposta (HTTP).

## Inicie uma consulta

```bash
curl \
  --header "Content-Type: application/json" \
  --request POST \
  --data '{"patient_id":"30774e9f-c105-4868-b83c-fbddd297c4b1","physician_id":"35d3aaab-edfd-4c47-8117-4a3d53417e7e","timestamp":"2020-12-01 13:00:00"}' \
  http://localhost:8000/appointments/start
```

Exemplo de resultado:

```json
{
  "id": "4178c3df-dc32-4bb7-947e-b713ab3b869b",
  "start_date": "2020-12-01T13:00:00",
  "end_date": null,
  "patient_id": "30774e9f-c105-4868-b83c-fbddd297c4b1",
  "physician_id": "35d3aaab-edfd-4c47-8117-4a3d53417e7e",
  "price": null
}
```

Comportamento:

- `gateway` recebe requisição (HTTP);
- `gateway` requisita inicio de consulta para `appointments` via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- `appointments` recebe mensagem de requisição;
- `appointments` cria consulta;
- `appointments` retorna consulta via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- `gateway` recebe consulta;
- `gateway` transmite consulta como resposta (HTTP).

## Finalize uma consulta

```bash
curl \
  --header "Content-Type: application/json" \
  --request POST \
  --data '{"timestamp":"2020-12-01 14:00:00"}' \
  http://localhost:8000/appointments/4178c3df-dc32-4bb7-947e-b713ab3b869b/end
```

Exemplo de resultado:

```json
{
  "id": "4178c3df-dc32-4bb7-947e-b713ab3b869b",
  "start_date": "2020-12-01T13:00:00",
  "end_date": "2020-12-01T14:00:00",
  "patient_id": "30774e9f-c105-4868-b83c-fbddd297c4b1",
  "physician_id": "35d3aaab-edfd-4c47-8117-4a3d53417e7e",
  "price": 200.00
}
```

Comportamento:

- `gateway` recebe requisição (HTTP);
- `gateway` requisita fim de consulta para `appointments` via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- `appointments` recebe mensagem de requisição;
- `appointments` atualiza consulta;
- `appointments` salva notificação de requisição de pagamento de consulta pendente no banco;
- `appointments` transmite evento de fim de consulta;
- [1] `appointments` retorna consulta via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- [1] `gateway` recebe consulta;
- [1] `gateway` transmite consulta como resposta (HTTP);
- [2] `payments` identifica evento de fim de consulta;
- [2] `payments` gera uma requisição de pagamento da consulta;
- [2] `payments` transmite evento de requisição de pagamento de consulta criada;
- [2.1] `appointments` identifica evento de criação de requisição de pagamento de consulta;
- [2.1] `appointments` remove notificação de requisição de pagamento de consulta pendente no banco.

Comportamento caso `payments` inativo:

- `gateway` recebe requisição (HTTP);
- `gateway` requisita fim de consulta para `appointments` via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- `appointments` recebe mensagem de requisição;
- `appointments` atualiza consulta;
- `appointments` salva notificação de requisição de pagamento de consulta pendente no banco;
- `appointments` transmite evento de fim de consulta;
- [1] `appointments` retorna consulta via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- [1] `gateway` recebe consulta;
- [1] `gateway` transmite consulta como resposta (HTTP);
- [2] `appointments` NÃO identifica evento de criação de requisição de pagamento de consulta;
- [2] `appointments` retransmite evento de fim de consulta;
- [.] `appointments` NÃO identifica evento de criação de requisição de pagamento de consulta;
- [.] `appointments` retransmite evento de fim de consulta;
- (Permanece no ciclo até `payments` transmitir evento).

## Verifique a notificação de cobrança

```bash
curl http://localhost:8000/appointments/4178c3df-dc32-4bb7-947e-b713ab3b869b/payment_request
```

Exemplo de resultado:

```json
{
  "appointment_id": "4178c3df-dc32-4bb7-947e-b713ab3b869b",
  "total_price": 200.00
}
```

Comportamento:

- `gateway` recebe requisição (HTTP);
- `gateway` requisita criação para `payments` via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- `payments` recebe mensagem de requisição;
- `payments` encontra requisição de pagamento da consulta;
- `payments` retorna requisição de pagamento da consulta via RPC, este que traduz para mensagem AMQP (RabbitMQ);
- `gateway` recebe requisição de pagamento da consulta;
- `gateway` transmite requisição de pagamento da consulta como resposta (HTTP).
