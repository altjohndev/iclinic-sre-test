# Instalação e Configuração

Conforme requisitos, a instalação é simplificada ao invés do uso de [docker](https://www.docker.com/) e [docker-compose](https://docs.docker.com/compose/).

Para mais informações de configuração via variável de ambiente, consulte o README do respectivo serviço:

- [appointments](../../apis/appointments/README.md)
- [gateway](../../apis/gateway/README.md)
- [patients](../../apis/patients/README.md)
- [payments](../../apis/payments/README.md)
- [physicians](../../apis/physicians/README.md)

O arquivo YAML abaixo define um exemplo de definição de serviços para uso via docker-compose, utilizando apenas um banco de dados:

```yaml
version: '3.8'

services:
  appointments:
    image: arkye/iclinic-sre-test:appointments
    environment:
      APPOINTMENTS_API__RABBITMQ_HOST: rabbitmq
      APPOINTMENTS_API__POSTGRES_HOST: postgres
    networks:
      - network
    depends_on:
      - postgres
      - rabbitmq

  gateway:
    image: arkye/iclinic-sre-test:gateway
    environment:
      GATEWAY_API__RABBITMQ_HOST: rabbitmq
    ports:
      - 8000:8000
    networks:
      - network
    depends_on:
      - rabbitmq

  patients:
    image: arkye/iclinic-sre-test:patients
    environment:
      PATIENTS_API__RABBITMQ_HOST: rabbitmq
      PATIENTS_API__POSTGRES_HOST: postgres
    networks:
      - network
    depends_on:
      - postgres
      - rabbitmq

  payments:
    image: arkye/iclinic-sre-test:payments
    environment:
      PAYMENTS_API__RABBITMQ_HOST: rabbitmq
      PAYMENTS_API__POSTGRES_HOST: postgres
    networks:
      - network
    depends_on:
      - postgres
      - rabbitmq

  physicians:
    image: arkye/iclinic-sre-test:physicians
    environment:
      PHYSICIANS_API__RABBITMQ_HOST: rabbitmq
      PHYSICIANS_API__POSTGRES_HOST: postgres
    networks:
      - network
    depends_on:
      - postgres
      - rabbitmq

  postgres:
    image: postgres:13.0
    environment:
      POSTGRES_DATABASES: "appointments,patients,payments,physicians,users"
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: password
    volumes:
      - ./docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d
      - postgres:/var/lib/postgresql/data
    ports:
      - 5432:5432
    networks:
      - network

  rabbitmq:
    image: rabbitmq:3.8.17-management
    ports:
      - 5672:5672
      - 15672:15672
    networks:
      - network

networks:
  network:
    name: iclinic_sre_test

volumes:
  postgres:
    name: iclinic_sre_test_postgres
```

Para criar mais de um banco no PostgreSQL em sua primeira inicialização, utilize do script abaixo e referencie em um volume para `/docker-entrypoint-initdb.d`:

```bash
set -e
set -u

function create_user_and_database() {
  echo "  Creating user and database '$1'"

  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER $1;
    CREATE DATABASE $1;
    GRANT ALL PRIVILEGES ON DATABASE $1 TO $1;
EOSQL
}

if [ -n "$POSTGRES_DATABASES" ]; then
  echo "Multiple database creation requested: $POSTGRES_DATABASES"

  for db in $(echo $POSTGRES_DATABASES | tr ',' ' '); do
    create_user_and_database $db
  done

  echo "Multiple databases created"
fi
```

Inicie os serviços:

```bash
docker-compose up -d
```

Siga o [Roteiro de aplicação](cookbook.md) para exemplos de uso.

## Para desenvolvimento e testes

Faça a build da imagem base para o projeto:

```bash
docker-compose -f .misc/docker/dev/build.yml build
```

Defina um docker-compose para desenvolvimento:

```yaml
version: '3.8'

services:
  dev:
    image: registry.gitlab.com/altjohndev/iclinic-sre-test:dev
    environment:
      APPOINTMENTS_API__RABBITMQ_HOST: rabbitmq
      APPOINTMENTS_API__POSTGRES_HOST: postgres
      APPOINTMENTS_API__POSTGRES_TEST_HOST: postgres
      APPOINTMENTS_API__POSTGRES_TEST_DB: appointments_test
      GATEWAY_API__RABBITMQ_HOST: rabbitmq
      PATIENTS_API__RABBITMQ_HOST: rabbitmq
      PATIENTS_API__POSTGRES_HOST: postgres
      PATIENTS_API__POSTGRES_TEST_HOST: postgres
      PATIENTS_API__POSTGRES_TEST_DB: patients_test
      PAYMENTS_API__RABBITMQ_HOST: rabbitmq
      PAYMENTS_API__POSTGRES_HOST: postgres
      PAYMENTS_API__POSTGRES_TEST_HOST: postgres
      PAYMENTS_API__POSTGRES_TEST_DB: payments_test
      PHYSICIANS_API__RABBITMQ_HOST: rabbitmq
      PHYSICIANS_API__POSTGRES_HOST: postgres
      PHYSICIANS_API__POSTGRES_TEST_HOST: postgres
      PHYSICIANS_API__POSTGRES_TEST_DB: physicians_test
    volumes:
      - ..:/app
      - bin:/usr/local/bin
      - python_modules:/usr/local/lib/python3.9/site-packages
    ports:
      - 8000:8000
    networks:
      - network
    depends_on:
      - postgres
      - rabbitmq

  postgres:
    image: postgres:13.0
    environment:
      POSTGRES_DATABASES:
        "appointments,\
        patients,\
        payments,\
        physicians,\
        users,\
        appointments_test,\
        patients_test,\
        payments_test,\
        physicians_test,\
        users_test"
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: password
    volumes:
      - ./postgres/docker-entrypoint-initdb.d:/docker-entrypoint-initdb.d
      - postgres:/var/lib/postgresql/data
    ports:
      - 5432:5432
    networks:
      - network

  rabbitmq:
    image: rabbitmq:3.8.17-management
    ports:
      - 5672:5672
      - 15672:15672
    networks:
      - network

networks:
  network:
    name: iclinic_sre_test

volumes:
  bin:
    name: iclinic_sre_test_bin

  postgres:
    name: iclinic_sre_test_postgres

  python_modules:
    name: iclinic_sre_test_python_modules
```

Para criar mais de um banco no PostgreSQL em sua primeira inicialização, utilize do script abaixo e referencie em um volume para `/docker-entrypoint-initdb.d`:

```bash
set -e
set -u

function create_user_and_database() {
  echo "  Creating user and database '$1'"

  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER $1;
    CREATE DATABASE $1;
    GRANT ALL PRIVILEGES ON DATABASE $1 TO $1;
EOSQL
}

if [ -n "$POSTGRES_DATABASES" ]; then
  echo "Multiple database creation requested: $POSTGRES_DATABASES"

  for db in $(echo $POSTGRES_DATABASES | tr ',' ' '); do
    create_user_and_database $db
  done

  echo "Multiple databases created"
fi
```

Para iniciar os serviços:

```bash
docker-compose up -d
```

Para acessar o ambiente de desenvolvimento:

```bash
docker-compose exec dev bash
```

Para realizar o teste completo do projeto no ambiente de desenvolvimento:

```bash
./test.sh
```
