#!/bin/bash

set -e
set -u

function create_user_and_database() {
  echo "  Creating user and database '$1'"

  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER $1;
    CREATE DATABASE $1;
    GRANT ALL PRIVILEGES ON DATABASE $1 TO $1;
EOSQL
}

if [ -n "$POSTGRES_DATABASES" ]; then
  echo "Multiple database creation requested: $POSTGRES_DATABASES"

  for db in $(echo $POSTGRES_DATABASES | tr ',' ' '); do
    create_user_and_database $db
  done

  echo "Multiple databases created"
fi
